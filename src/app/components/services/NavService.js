(function(){
  'use strict';

  angular.module('app')
          .service('navService', [
          '$q',
          navService
  ]);

  function navService($q){
    var menuItems = [
      {
        name: 'ראשי',
        icon: 'dashboard',
        sref: '.dashboard'
      },
      {
        name: 'פרופיל',
        icon: 'person',
        sref: '.profile'
      },
      {
        name: 'מחולל דוחות',
        icon: 'view_module',
        sref: '.table'
      },
      {
        name: 'הנהלת חשבונות',
        icon: 'view_module',
        sref: '.data-table'
      }
    ];

    return {
      loadAllItems : function() {
        return $q.when(menuItems);
      }
    };
  }

})();
