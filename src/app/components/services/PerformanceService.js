(function () {
    'use strict';

    angular.module('app')
        .service('performanceService', [
            performanceService
        ]);

    function performanceService() {

        var service = {
            getPerformanceData: getPerformanceData
        };

        return service;

        function getPerformanceData(performancePeriod) {
            return [
                {
                    "key": 'Middleware',
                    "values": [[1, 11], [2, 10], [3, 14], [4, 21], [5, 13], [6, 21], [7, 21], [8, 18], [9, 11], [10, 11], [11, 18], [12, 14], [13, 10], [14, 20], [15, 21], [16, 28], [17, 12], [18, 16], [19, 22], [20, 18], [21, 21], [22, 10], [23, 11], [24, 14], [25, 9], [26, 14], [27, 10], [28, 21], [29, 11]]
                },

                {
                    "key": 'Ruby',
                    "values": [[1, 29], [2, 36], [3, 42], [4, 25], [5, 22], [6, 34], [7, 41], [8, 19], [9, 45], [10, 31], [11, 28], [12, 36], [13, 54], [14, 41], [15, 36], [16, 39], [17, 21], [18, 20], [19, 22], [20, 44], [21, 32], [22, 20], [23, 28], [24, 24], [25, 29], [26, 19], [27, 20], [28, 31], [29, 49]]
                }
            ]
        }
    }
})();
