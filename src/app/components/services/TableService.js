﻿(function(){
  'use strict';

  angular.module('app')
        .service('tableService', [
        '$q',
      tableService
  ]);

  function tableService($q){
    var tableData = [
        {
            name: 'אבטחת מידע',
            hp: 100,
            number: 123764,
            describe: 'סחורה',
            done_stones: 'Done',
            stones: 'תשלום',
            done_date: '21.04.18',
            done_paid: 200300,
            hov: 20100,
            sum_hov: '41000'
        },
        {
            name: 'אבטחת מידע',
            hp: 100,
            number: 123764,
            describe: 'סחורה',
            done_stones: 'Done',
            stones: 'תשלום',
            done_date: '21.04.18',
            done_paid: 200300,
            hov: 20100,
            sum_hov: '41000'
        },
        {
            name: 'אבטחת מידע',
            hp: 100,
            number: 123764,
            describe: 'סחורה',
            done_stones: 'Done',
            stones: 'תשלום',
            done_date: '21.04.18',
            done_paid: 200300,
            hov: 20100,
            sum_hov: '41000'
        },
        {
            name: 'אבטחת מידע',
            hp: 100,
            number: 123764,
            describe: 'סחורה',
            done_stones: 'Done',
            stones: 'תשלום',
            done_date: '21.04.18',
            done_paid: 200300,
            hov: 20100,
            sum_hov: '41000'
        },
        {
            name: 'אבטחת מידע',
            hp: 100,
            number: 123764,
            describe: 'סחורה',
            done_stones: 'Done',
            stones: 'תשלום',
            done_date: '21.04.18',
            done_paid: 200300,
            hov: 20100,
            sum_hov: '41000'
        }
    ];

    function PickRandom() {
      return Object.assign({}, tableData[Math.floor(Math.random()*tableData.length)]);
    }

    return {
      loadAllItems : function() {
        return $q.when(tableData);
      },
      /**
       * Query expects that `limit`,`page`, and `order` fields be present
       */
      loadByPagination: function (query) {
        query = query || {limit:10,page:1};
         
        var list = [];
        var start = (query.page-1)*query.limit;
        var end = start + query.limit;
        for (var i = start; i < end; i++) {
          var f = PickRandom();
          f.id = i+1;
          list.push(f);
        }
        return $q.when({items:list,count:800});
      }
    };
  }
})();
